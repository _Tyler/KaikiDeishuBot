### Paths

- Docs
    - [Guide](docs/GUIDE.md)
    - [.env setup](docs/ENV.md)

# Welcome to Kaiki Deishu Bot

KaikiBot is a personal project for me and my server. Feel free to selfhost, fork, support this project!

[Add Kaiki to your server](https://discord.com/oauth2/authorize?client_id=714695773534814238&scope=bot)

# Guide

[Selfhosting guide](docs/GUIDE.md)

# Contributing

Consider contributing. I appreciate all the help I can get!

1. Fork the bot!
1. Create your feature branch: `git checkout -b cool-new-branch`
1. Commit your changes: `git commit -am "Added x feature!"`
1. Push to the branch: `git push origin My-new-feature`
1. Submit a pull request!

# About

### I owe some amazing people thanks, and more!

- Huge thanks to @Arvfitii for helping me whenever im in need!
- Thanks to @rjt-rockx on Discord for so much help and time!
- Should also mention @shivaco for help <3
- Thanks also to @Kwoth <3

### A bit about this project

This project was my first entry into FOSS and programming/coding overall.

It has been very fun and also frustrating.

If you do find unoptimized code and or flaws or other inherently bad code - I would be very happy to merge changes and
try to learn from my own mistakes. Commenting the code helps! I will try to do so as well.
