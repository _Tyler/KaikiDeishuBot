export enum BlockedCategoriesEnum {
    Administration = 0,
    Anime,
    Emotes,
    Fun,
    Gambling,
    Moderation,
    NSFW,
    "Owner only",
    Reactions,
    Roles,
    "Server settings",
    Utility,
}
