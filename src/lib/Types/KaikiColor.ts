export type KaikiColor = {
    r: number;
    g: number;
    b: number;
};
