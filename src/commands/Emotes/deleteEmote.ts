import { Collection, EmbedBuilder, GuildEmoji, Message, PermissionsBitField } from "discord.js";
import KaikiCommand from "../../lib/Kaiki/KaikiCommand";
import KaikiEmbeds from "../../lib/KaikiEmbeds";
import Utility from "../../lib/Utility";
import Constants from "../../struct/Constants";

const timer = (ms: number) => new Promise(res => setTimeout(res, ms));

export default class DeleteEmoteCommand extends KaikiCommand {
    constructor() {
        super("deleteemote", {
            aliases: ["deleteemote", "de"],
            description: "Deletes one or multiple emotes/emoji. Multiple emotes take longer, to avoid ratelimits. Keep a space between all emotes you wish to delete.",
            usage: "<:NadekoSip:>",
            clientPermissions: PermissionsBitField.Flags.ManageEmojisAndStickers,
            userPermissions: PermissionsBitField.Flags.ManageEmojisAndStickers,
            channel: "guild",
            typing: true,
            args: [
                {
                    id: "emotes",
                    match: "separate",
                    type: "emojis",
                    otherwise: (msg: Message) => ({ embeds: [KaikiEmbeds.genericArgumentError(msg)] }),
                },
            ],
        });
    }

    public async exec(message: Message, { emotes }: { emotes: Collection<string, GuildEmoji>[] }): Promise<Message> {

        return (async function() {
            let i = 0;
            for (const emote of emotes) {

                const newEmote = message.guild?.emojis.cache.get(emote.map(e => e.id)[0]);

                if (newEmote) {

                    i > 0 ? await timer(Constants.MAGIC_NUMBERS.CMDS.EMOTES.DELETE_EMOTE.DELETE_DELAY) && i++ : i++;

                    const deleted = await newEmote.delete();

                    if (!deleted) {
                        return message.channel.send({
                            embeds: [
                                new EmbedBuilder({
                                    title: "Error occurred",
                                    description: "Some or all emotes could not be deleted.",
                                })
                                    .withErrorColor(message),
                            ],
                        });
                    }
                }

                else {
                    return message.channel.send({
                        embeds: [
                            new EmbedBuilder({
                                title: "Error occurred",
                                description: "Not valid emote(s).",
                            })
                                .withErrorColor(message),
                        ],
                    });
                }
            }

            return message.channel.send({
                embeds: [
                    new EmbedBuilder()
                        .setTitle("Success!")
                        .setDescription(`Deleted:\n${Utility.trim(emotes.map((es) => es.map((e) => e)).join("\n"), Constants.MAGIC_NUMBERS.EMBED_LIMITS.DESCRIPTION)}`)
                        .withOkColor(message),
                ],
            });
        })();
    }
}
