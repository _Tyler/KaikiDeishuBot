import { Argument } from "discord-akairo";
import { sendPaginatedMessage } from "discord-js-button-pagination-ts";
import { EmbedBuilder, GuildMember, Message, Role, TextChannel } from "discord.js";
import KaikiCommand from "../../lib/Kaiki/KaikiCommand";
import Utility from "../../lib/Utility";

export default class CheckPermissionsCommand extends KaikiCommand {
    constructor() {
        super("checkpermissions", {
            aliases: ["checkperms", "cp", "perms"],
            description: "Lists perms for role/member",
            usage: ["", "@user", "@role", "@user #channel"],
            channel: "guild",
            args: [
                {
                    id: "input",
                    type: Argument.union("role", "member"),
                    default: (message: Message) => message.member,
                },
                {
                    id: "channel",
                    type: "textChannel",
                    default: (message: Message) => message.channel,
                },
            ],
            subCategory: "Info",
        });
    }

    public async exec(message: Message, {
        input,
        channel,
    }: { input: Role | GuildMember, channel: TextChannel }): Promise<Message> {

        const { permissions } = input,
            permissionsIn = input.permissionsIn(message.channel as TextChannel),
            inputName = input instanceof Role ? input.name : input.user.tag;

        const pages = [];

        if (permissionsIn.bitfield !== permissions.bitfield) {
            pages.push(new EmbedBuilder()
                .withOkColor(message)
                .setTitle(`Permissions for ${inputName} in #${channel.name}`)
                .setDescription(await Utility.codeblock(permissionsIn
                    .toArray()
                    .sort()
                    .join("\n"))),
            );

            if (message.channel.id !== channel.id) {
                return message.channel.send({ embeds: [pages[0]] });
            }
        }

        else if (message.channel.id !== channel.id) {
            pages.push(new EmbedBuilder()
                .withOkColor(message)
                .setTitle(`Permissions for ${inputName} in #${channel.name}`)
                .setDescription(await Utility.codeblock(permissionsIn
                    .toArray()
                    .sort()
                    .join("\n"))),
            );
        }

        else {
            pages.push(new EmbedBuilder()
                .withOkColor(message)
                .setTitle(`General permissions for ${inputName}`)
                .setDescription(await Utility.codeblock(permissions
                    .toArray()
                    .sort()
                    .join("\n"))),
            );
        }

        return sendPaginatedMessage(message, pages, {});
    }
}
